{% extends "cpp_header.h" %}

{% block include_guard_open %}
#ifndef {% include "include_guard_cuda.txt" %}
#define {% include "include_guard_cuda.txt" %}
{% endblock include_guard_open %}

{% block namespaces_close %}
{% include "namespace_close_cpp_named.txt" %}
{% endblock namespaces_close %}


{% block include_guard_close %}
#endif // {% include "include_guard_cuda.txt" %}
{% endblock include_guard_close %}
