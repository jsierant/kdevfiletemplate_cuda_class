{% extends "cpp_implementation.cpp" %}
{% load kdev_filters %}

{% block includes %}
{{ block.super }}


//cuda includes
#include <driver_functions.h>
{% endblock includes %}

{% block namespaces_use %}
{% include "namespace_open_cpp.txt" %}
{% endblock namespaces_use %}


{% block function_definitions %}
{{ block.super }}


{% include "namespace_close_cpp_named.txt" %}
{% endblock function_definitions %}